import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { PeopleComponent } from './views/people/people.component';
import { GroupsComponent } from './views/groups/groups.component';
import { FamiliesComponent } from './views/families/families.component';
import { UsersComponent } from './views/users/users.component';
import { SettingsComponent } from './views/settings/settings.component';
import { EventsComponent } from './views/events/events.component';
import { MaterialModule } from './material.module';
import { NotFoundComponent } from './views/not-found/not-found.component';
import { PageTitleComponent } from './common/page-title/page-title.component';
import { ListTopComponent } from './common/list-top/list-top.component';

const routes: Routes = [
  { path: 'people', component: PeopleComponent, data: { title: 'People' } },
  { path: 'groups', component: GroupsComponent, data: { title: 'Groups' } },
  { path: 'families', component: FamiliesComponent, data: { title: 'Families' } },
  { path: 'users', component: UsersComponent, data: { title: 'Users' } },
  { path: 'settings', component: SettingsComponent, data: { title: 'Settings' } },
  { path: 'events', component: EventsComponent, data: { title: 'Events' } },
  { path: '404', component: NotFoundComponent, data: { title: 'Not Found' } },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  declarations: [
    PeopleComponent,
    GroupsComponent,
    FamiliesComponent,
    UsersComponent,
    SettingsComponent,
    EventsComponent,
    NotFoundComponent,
    PageTitleComponent,
    ListTopComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [
    RouterModule,
    MaterialModule
  ],
  providers: [ ]
})
export class MainModule { }
