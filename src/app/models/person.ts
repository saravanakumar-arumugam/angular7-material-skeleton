export class Person {
    constructor(
        public Id?: number,
        public Gender?: number,
        public Title?: string,
        public Name?: string,
        public DOB?: Date,
        public Phone?: string,
        public Mobile?: string,
        public Email?: string,
        public SubscriptionNumber?: number,
        public MembershipDate?: Date
    ) {}
}