import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-list-top',
  templateUrl: './list-top.component.html',
  styleUrls: ['./list-top.component.scss']
})
export class ListTopComponent implements OnInit {

  @Input() addButtonTitle: string = '';
  @Input() onAddClick: Function = function () { console.log("Add button event not specified"); };

  constructor() { }

  ngOnInit() {
  }
}
