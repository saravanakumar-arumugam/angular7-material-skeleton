import { Router } from '@angular/router';
import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @ViewChildren('drawer') dc: QueryList<MatSidenav>;

  private showMenu: boolean = true;

  private menus = [
    {
      title: "People",
      icon: "person",
      link: "/people"
    },
    {
      title: "Families",
      icon: "class",
      link: "/families"
    },
    {
      title: "Groups",
      icon: "group_work",
      link: "/groups"
    },
    {
      title: 'Settings',
      icon: 'settings_applications',
      link: '/settings'
    },
    {
      title: 'Users',
      icon: 'person_outline',
      link: '/users'
    },
    {
      title: 'Logout',
      icon: 'power_settings_new',
      link: '/logout'
    },
  ];

  constructor(private router: Router) {}

  ngOnInit() {
  }

  onActivate() {
    this.showMenu = this.router.routerState.snapshot.url != '/404';
  }

}
